from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification
from scipy.special import softmax
from flask import Flask, request, jsonify
import json

app = Flask(__name__)


@app.post("/")
def hello_world():

    inputtedData = request.json
    text = inputtedData["text"] if "text" in inputtedData else None
    
    MODEL = f"cardiffnlp/twitter-roberta-base-sentiment" #PretrainedModel
    tokenizer = AutoTokenizer.from_pretrained(MODEL)
    model = AutoModelForSequenceClassification.from_pretrained(MODEL)

    text_encoded=tokenizer(text,return_tensors='pt') 
    output = model(**text_encoded)

    scores = output[0][0].detach().numpy() 
    scores = softmax(scores) 
    
    return jsonify(roberta_neg=str(scores[0]), roberta_neu = str(scores[1]), roberta_pos = str(scores[2]))



if __name__ == '__main__':
      app.run(host='0.0.0.0', port=5000)







 